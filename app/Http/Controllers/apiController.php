<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class apiController extends Controller {

  use AuthenticatesUsers;
  
  public function deleteUser($user) {
    
    \App\User::where('id', '=', intval($user))->delete();
    
    return ['success' => true, 'result' => intval($user)];
  }
  
  public function updateUser(Request $request, $user) {
    
    $fields = ['name', 'password'];
    $patch  = [];
    
    foreach($fields as $field)
      if($request->has($field))
        $patch[$field]  = ($field != 'password' ? $request->{$field} : Hash::make($request->{$field}));
    
    \App\User::where('id', '=', intval($user))->update($patch);
    
    return ['success' => true, 'result' => intval($user)];
  }
  
  public function addUser(Request $request) {
    
    $login  = trim($request->post('login'));
    $name = trim($request->post('name'));
    $pass = trim($request->post('pass'));
    
    if($name && $pass) {
      
      $user = \App\User::create([
                                  'name' => $name,
                                  'email' => $login,
                                  'password' => Hash::make($pass),
                                  'api_token' => Str::random(60),
                                ]);
      if($user)
        return ['success' => true, 'result' => 'User created successfully'];
    }
    
    return response()->json(['success' => false, 'result' => 'Username or pass empty']);
  }
  
  public function loginUser(Request $request) {
    
    $login  = trim($request->post('login'));
    $pass = trim($request->post('pass'));
    
    if($this->guard()->once(['email' => $login, 'password' => $pass])) {
      
      $token  = Str::random(60);
      
      $this->guard()->user()->update(['api_token' => $token]);//
      
      return response()->json(['success' => true, 'result' => ['token' => $token, 'id' => $this->guard()->user()->id]]);
    }
    
    return response()->json(['success' => false, 'result' => 'Auth failed']);
  }
  
  public static function listUsers(Request $request) {

    $chunk  = 5;
    $total  = \App\User::count();
    $pages  = max(0, ceil($total / $chunk) - 1);
    $pagination = null;
    $currentPage  = min(intval($request->post('page')), $pages);

    if($pages) {
      
      $pagination = [
        'first' => 0,
        'middle' => [],
        'last' => $pages
      ];
      
      for($i = max(1, $currentPage - 3); $i <= min($pages - 1, $currentPage + 3); $i++)
        array_push($pagination['middle'], $i);
    }
    
    return response()->json(['success' => true, 'result' => ['pagination' => $pagination, 'list' => \App\User::skip($chunk * $currentPage)->take($chunk)->select(['email as login', 'name', 'id'])->get()]]);
  }
}
